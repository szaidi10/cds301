clear
%A = [1.001, 2.0, 3.0, -1.000, 0, 3]
A = [0.1,0.2,0.3]
% Create a sum variable that will be incremented on each iteration.
s = 0;
count = 0;
% Iterate over each value in A
for i = 1:length(A)
    % Add previous value of s to A(i)
    % A(i) ~= -1:1 is comparing a scalar with the array -1, 0, 1.
    % It is not working.
    % Try
    % if (A(i) >= 1 || A(i) <=-1)
    if A(i) ~= -1:1 
    	s = s + A(i)
    	count = count + 1
    end  
end
Average = s/count