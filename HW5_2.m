clear;
X = [ 1  ,   2  ;  1  ,  2]
Y = [ 1  ,   1  ;  2  ,  2]
H = [10  , 30   ; 20  , 40]
T = [94 , 96 ; 95, 97]
% Estimate height and temperature at (x,y) = (1.5,1.5)
% X, Y, and H are needed in order to make the estimate.
He = interp2(X,Y,H,1.5,1.5)
Hi = interp2(X,Y,T,1.5,1.5) 