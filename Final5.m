clear;
clf;
x = load('particles3.txt');
particle1posx = x(:,2);
particle1posy = x(:,3);
particle1velx = x(:,4);
particle1vely = x(:,5);
particle2posx = x(:,6);
particle2posy = x(:,7);
particle2velx = x(:,8);
particle2vely = x(:,9);
particle3posx = x(:,10);
particle3posy = x(:,11);
particle3velx = x(:,12);
particle3vely = x(:,13);

% I like your approach.  It needs a bit of tweaking, but is the right
% idea.  See my approach at http://mag.gmu.edu/git-data/cds301/tmp

for i = 1:length(particle1posx);
    d = (particle1velx(i).^2 + particle1vely(i).^2)/4;
    e = (particle2velx(i).^2 + particle2vely(i).^2)/4;
    f = (particle3velx(i).^2 + particle3vely(i).^2)/4;
    hold on;
    Ca = [d d d]
    Ca1 = [e e d];
    Ca2 = [f f f];
    plot (particle1posx(i), particle1posy(i), '.','MarkerSize', 40, 'Color', Ca);
    plot (particle2posx(i), particle2posy(i), '.','MarkerSize', 40, 'Color', Ca1);
    plot (particle3posx(i), particle3posy(i), '.','MarkerSize', 40, 'Color', Ca2);
    legend('Particle 1', 'Particle2', 'Particle3');
    axis([0 5 0 5]);
    grid on;
    c=colorbar;
    caxis([0 1]);
    colormap('gray');
    xlabel('Horizontal Position [X]');
    ylabel('Vertical Position[Y]');
    title('Position Over Time')
    pause(0.1);
    clf;
end
