clear;
r1=randi(100,1, 10)
A=r1
for i = 2:size(r1,2)-1
        A(i) = 0.33 * A(i-1) + 0.33 * A(i) + 0.33 * A(i+1)
        % Technically, you should do 
        % (A(i-1) + A(i) + A(i+1))/3.  I'll accept as this
        % is what I did for an example where A was uint8 and
        % it was needed to get a sensible answer.
end
r1
A
