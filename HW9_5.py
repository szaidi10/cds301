for s in GetSources().values():
    Delete(s)

renderView1 = GetActiveViewOrCreate('RenderView')

# Create a sphere object
sphere1 = Sphere()
# Create a sphere display object
sphere1Display = Show(sphere1, renderView1)

for frame in range(1,11):
    # Set the x and y values of the center of the sphere based on the frame number
    sphere1.Center = [0.0,0.0,frame/10.0]
    # Show the updated sphere
    RenderAllViews()
    # Create a filename based on the frame number
    fname = "animation_%02d.png" % frame
    # Save the frame as a png file
    WriteImage(fname)
