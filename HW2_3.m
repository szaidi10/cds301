clear;
A=imread('cameraman.tif');
figure(1);
imshow(A)
figure(1); % Any plotting commands that follow will appear in a window named figure 1.
  imshow(A)
As = A; % Copy original matrix

% Do averaging
for i = 1:size(A,1)
	for j = 2:size(A,2)-1 % Can't start at j=1 because no element to left to use.
		% When j = 2, calculation is As(i,2) = (A(i,1)+A(i,2)+A(i,3))/3
		As(i,j) = 0.33*A(i,j-1) + 0.33*A(i,j) + 0.33*A(i,j+1);
	end
end
% Visually inspect row 1, columns 1 through 4
A(1,1:4)
As(1,1:4)

figure(2);  % Any plotting commands that follow will appear in a window named figure 2.
  imshow(As)