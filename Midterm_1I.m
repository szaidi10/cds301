clear;clf;
% Read image cameraman.tif into a matrix I (cameraman.tif comes with MATLAB)
A = imread('cameraman.tif');
Anew = A
% Convert values to double precision (original are unit8)
% This is done so that math with matrix elements works as expected (e.g., 255+5 = 260)
% If we did not take this step, the result of adding elements with values of 255 and 5 would be 255.
Anew = double(Anew); 
% Plot the matrix
A=Anew(1:2:end, 1:2:end);
colormap('gray');
image(Anew);
colorbar;
whos('A')
