clear;
x = [0:20:100]; % An array of x coordinates.
y = [0:20:100]; % An array of y coordinates.

% Create a matrix of altitude values.
for i = 1:length(x)
    for j = 1:length(y)
        z(i,j) = (x(i)-50)^2 + (y(j)-50)^2;
    end
end
z = z/1000;

% Open a figure window if it does not exist and then clear it.
figure(1);clf;

% Set axis limits wider than default
axis([-20 120 -20 120]); 

% Each time a plot command is issued, MATLAB clears the figure window
% unless hold on was executed.
hold on;

% Draw a dot at position x(i), y(i).
% Display text at position x(i), y(i).
% Offset text by 2 units to it does not overlap dot.
for i = 1:length(x)
    for j = 1:length(y)
        plot(x(i),y(j),'b.','MarkerSize',20);
        text(2+x(i),2+y(j),['z=',num2str(z(i,j))],'Color','blue');
    end
end

% Add axes labels.
ylabel(gca, 'y','Rotation',0);
xlabel(gca,'x');

% Make the aspect ratio of the axes square.
axis square

% Set tick locations manually.
set(gca,'XTick',[0:20:100])
set(gca,'YTick',[0:20:100])

% Show a background grid.
grid on;

% Add a label indicating the function that is plotted.
text(25,114,'Values of z=[(x-50)^2+(y-50)^2]/1000')

% Draw contour lines for z = 1.
for i = 2:length(x)-1
    for j = 2:length(y)-1
            % If above condition is true, display some information.
            fprintf('z(%d,%d) = 1 (x=%d,y=%d)\n',i,j,x(i),y(j));
                % If above two conditions are true, display some information.
            
                fprintf('   Left: z(%d,%d) = 1 (x=%d,y=%d)\n',i-1,j,x(i-1),y(j));
                %plot([x(i),(x(i)+x(i-1))/2],[y(j),(y(j)+y(j-1))/2]); % Draw line that connects.
                if z(i,j)<1.5 && z(i-1,j)>1.5
                    plot((x(i)+x(i-1))/2,y(j),'m.','MarkerSize',30);
                    text(x(i),y(j),['z=',num2str(z(i,j))],'Color','red');    
                end
                fprintf('   Right: z(%d,%d) = 1 (x=%d,y=%d)\n',i+1,j,x(i+1),y(j));
                %plot([x(i),(x(i)+x(i+1))/2],[y(j),y(j)]); % Draw line that connects.
                if z(i,j)<1.5 && z(i+1,j)>1.5
                    plot((x(i)+x(i+1))/2,y(j),'m.','MarkerSize',30);
                    text((x(i))/2,y(j),['z=',num2str(z(i,j))],'Color','red');
                end
                fprintf('   Top: z(%d,%d) = 1 (x=%d,y=%d)\n',i,j+1,x(i),y(j+1));
                %plot([x(i),x(i)],[y(j),(y(j)+y(j+1))/2]); % Draw line that connects.
                if z(i,j)<1.5 && z(i,j+1)>1.5
                    plot(x(i),(y(j)+y(j+1))/2,'m.','MarkerSize',30);
                    text(x(i),(y(j))/2,['z=',num2str(z(i,j))],'Color','red');
                end
                fprintf('   Bottom: z(%d,%d) = 1 (x=%d,y=%d)\n',i,j-1,x(i),y(j-1));
                %plot([x(i),x(i)],[y(j),(y(j)+y(j-1))/2]); % Draw line that connects.
                if z(i,j)<1.5 && z(i,j-1)>1.5
                    plot(x(i),(y(j)+y(j-1))/2,'m.','MarkerSize',30);
                    text(x(i),(y(j))/2,['z=',num2str(z(i,j))],'Color','red'); 
                end 
    end
end

% Don't worry about understanding this part.  It just makes
% the labels look sensible.
% Add index axes.
% Based on technique from http://stackoverflow.com/questions/19569134/overlaying-two-axes-in-a-matlab-plot
% Create a new axes in the same position as the first one, overlaid on top
h2 = axes('position', get(gca, 'position')); 
% Put the new axes' y labels on the right, set the x limits the same as the
% original axes', and make the background transparent
set(h2, 'YAxisLocation', 'right', 'YLim',[0,length(y)+1],'Xlim', [0,length(x)+1], 'Color', 'none'); 
ylabel(h2, 'j','Rotation',0);
xlabel(h2, 'i');
set(h2,'XTick',[1:length(x)])
set(h2,'YTick',[1:length(y)])
set(h2, 'XAxisLocation', 'top');
axis square;
hold on;

print -dpng contour_algorithm_basics.png
print -deps contour_algorithm_basics.eps