clear;
r1 = rand(10,10);
A=r1
for i = 2:size(r1,1)-1
    for j = 2:size(r1,2)-1
        A(i,j) = 0.20 * A(i-1,j) + 0.20 * A(i,j) + 0.20 * A(i+1,j)+ 0.20 * A(i,j+1)+ 0.20 * A(i, j-1)
    end
end
% Note that calculation will require fewer operations (and be faster)
% if you do only one division by 5 instead of 5 multiplications by 0.20.
r1
A