clear;clf;
frame = 0;
for t=[0:0.01:0.41]
    frame = frame+1
    xcoord = 1
    ycoord = 1
    Xpos(xcoord) = 2*t;
    Ypos(ycoord) = 2*t-4.9*t.^2;
    plot(Xpos(xcoord),Ypos(ycoord),'.','MarkerSize',20);

   % hold on tells MATLAB to not clear figure when next plot command issued.
   %hold on; 

   % drawnow tells MATLAB to render immediately.  If this is not done,
   % and plots happen quickly, MATLAB may wait until things slow down
   % before rendering on the screen - an you may only see the last frame.
   drawnow; 

   % Uncomment the following to make MATLAB wait until you 
   % hit enter to show the next plot.  Useful for debugging.
   %input('Continue?');

   % Set axis limits and labels.  See help axis.
   axis([0 1 0 0.25]);
   xlabel('x position')
   ylabel('y position')

   % Create a filename for the image.  %02d means "a 2 digit integer that is
   % zero padded, e.g., 01, 02, 03, ...".  No semicolon is given at end
   % so file that is being written is displayed in the console.
   fname = sprintf('ballposition_%02d.png', frame); % Only one template (%02d), so only one number needed.  Also, the value of xcoord is always one in your loop.  Need it to increase with iteration number.

   % "print" the image to a file in png format (the -d means "device").
   % Both words "print" and "device" are anachronisms.
   print('-dpng',fname);
   xcoord = xcoord+1
   ycoord = ycoord+1
end