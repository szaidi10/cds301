#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'Cylinder'
cylinder1 = Cylinder()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [844, 548]

# show data in view
cylinder1Display = Show(cylinder1, renderView1)
# trace defaults for the display properties.
cylinder1Display.ColorArrayName = [None, '']

# reset view to fit data
renderView1.ResetCamera()

# change solid color
cylinder1Display.DiffuseColor = [0.0, 0.0, 1.0]

# create a new 'Sphere'
sphere1 = Sphere()

# show data in view
sphere1Display = Show(sphere1, renderView1)
# trace defaults for the display properties.
sphere1Display.ColorArrayName = [None, '']

# change solid color
sphere1Display.DiffuseColor = [1.0, 0.0, 0.0]

# Properties modified on sphere1
sphere1.Center = [1.0, 1.0, 1.0]

# Properties modified on sphere1
sphere1.Center = [0.0, 1.0, 0.0]
