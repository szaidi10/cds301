% See solutions
clear;
clf;
% Read image cameraman.tif into a matrix I (cameraman.tif comes with MATLAB)
A = imread('cameraman.tif');
Mnew = A
% Convert values to double precision (original are unit8)
% This is done so that math with matrix elements works as expected (e.g., 255+5 = 260)
% If we did not take this step, the result of adding elements with values of 255 and 5 would be 255.
Mnew = double(Mnew);
% Plot the matrix
for i = 1:size(A,1)
    for j = 1:2:size(A,2)
        Mnew(i,j) = 0.5*A(i,j) + 0.5*A(i,j+1);
    end
end
Mnew3 = Mnew
for x = 1:2:size(Mnew,1)
    for y = 1:size(Mnew,2)
        Mnew3(x,y) = 0.5*Mnew(x,y) + 0.5*Mnew(x+1, y);
        Mnew2=Mnew3(1:2:end, 1:2:end);
    end
end

colormap('gray');
image(Mnew);
colorbar;
