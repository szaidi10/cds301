clear;
theta = [0:45:360];
theta2 = [0:45:360];
for i = 1:length(theta)
    x(i) = cosd(theta(i));
    y(i) = sind(theta(i));
end
for j = 1:length(theta2)
    a(j) = cosd(theta2(j));
    b(j) = sind(theta2(j));
end
fid = fopen('HW8_2.vtk','w');
                                                                                               
fprintf(fid,'# vtk DataFile Version 3.0\n');                                                     
fprintf(fid,'Circle\n');                                                                   
fprintf(fid,'ASCII\n');                                                                            

fprintf(fid,'\nDATASET POLYDATA\n');
fprintf(fid,'POINTS 18 float\n');                                                      
for i = 1:length(x)                                                                            
  fprintf(fid,'%f %f %f\n',x(i),y(i),0);                                                         
end 
for j = 1:length(a)                                                                            
  fprintf(fid,'%f %f %f\n',a(j),b(j),2);                                                         
end 
fclose(fid);
type HWK8_2.vtk

