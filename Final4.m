clear;
clf;
x = load('particles3.txt');
particle1posx = x(:,2);
particle1posy = x(:,3);
particle2posx = x(:,6);
particle2posy = x(:,7);
particle3posx = x(:,10);
particle3posy = x(:,11);
for i = 1:length(particle1posx);
    hold on;
    plot (particle1posx(i), particle1posy(i), '.','MarkerSize', 40, 'Color', [1 0 0]);
    plot (particle2posx(i), particle2posy(i), '.','MarkerSize', 40, 'Color', [0 1 0]);
    plot (particle3posx(i), particle3posy(i), '.','MarkerSize', 40, 'Color', [0 0 1]);
    legend('Particle 1', 'Particle2', 'Particle3')
    axis([0 5 0 5]);
    grid on;
    xlabel('Horizontal Position [X]');
    ylabel('Vertical Position[Y]');
    title('Position Over Time')
    pause(0.1);
    clf;
end
