clear;
C=[1,0,0;0,1,0;0,0,1]
A = imread('cameraman.tif');

figure(1);
imshow(A);                % Display the original image.
print -dpng cameraman.png % Save original image.

imagesc(A);
colormap(C);
colorbar;

figure(2);
imshow(A); % Display the modified image.
print -dpng cameraman_dithered.png % Save the modified image.