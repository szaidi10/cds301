clear;

%Each row is a (x,y,z) value for a point.
P = rand(1000,3);
Npts = size(P,1);

outfile = 'HW8_1.vtk';

%Create and open a file named outfile for writing ('w').
fid = fopen(outfile,'w');

fprintf(fid,'# vtk DataFile Version 3.0\n');
fprintf(fid,'A dataset with one polyline and no attributes\n');
fprintf(fid,'ASCII\n');
fprintf(fid,'\n');
fprintf(fid,'DATASET POLYDATA\n');
fprintf(fid,'POINTS %d float\n',Npts);
for i = 1:Npts
    fprintf(fid,'%f %f %f\n',P(i,1),P(i,2),P(i,3));
end
j=rand(1000,1);
fprintf(fid,'\nPOINT_DATA %d\n',Npts);
fprintf(fid,'SCALARS point_scalars float\n');
fprintf(fid,'LOOKUP_TABLE default\n');
for i = 1:1000
    fprintf(fid,'%f\n',j(i,1))
end
fprintf(fid,'\n');
fclose(fid);
type(outfile);
