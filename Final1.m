clear;
clf;
urlwrite('http://mag.gmu.edu/git-data/cds301/md2/data/particles3.txt','particles3.txt');
x = load('particles3.txt');
time = x(:,1)
particle1 = x(:,3)
particle2 = x(:,7)
particle3 = x(:,11)

a = plot(time, particle1, 'Color', 'Green', 'LineWidth', 3)
hold on;
b = plot(time, particle2, 'Color', 'Red', 'LineWidth', 3)
hold on;
c = plot(time, particle3, 'Color', 'Blue', 'LineWidth', 3)
hold on;
axis([0 2.0 1.5 4.0])
grid on;
n=get(gca,'xtick');
%set(gca,'xticklabel',sprintf('%.1f |',n'));
set(gca,'xticklabel',sprintf('%.1f\n',n'));
% Use SI abbreviation.
xlabel('Time [s]');
ylabel('Y Position [m]');
title('Y Position vs. Time')
legend('Particle1', 'Particle2', 'Particle3', 'location', 'SouthWest');
