clear;
clf;
x = load('particles3.txt');
particle1posx = x(:,2)
particle1posy = x(:,3)
particle2posx = x(:,6)
particle2posy = x(:,7)
particle3posx = x(:,10)
particle3posy = x(:,11)

a = plot (particle1posx(1), particle1posy(1), '.', 'MarkerSize', 25, 'Color', 'Green')
hold on;
b = plot (particle2posx(1), particle2posy(1), '.', 'MarkerSize', 25, 'Color', 'Red')
hold on;
c = plot (particle3posx(1), particle3posy(1), '.', 'MarkerSize', 25, 'Color', 'Blue')
hold on;
axis([1 4 2 4])
grid on;
xlabel('Horizontal Position [X]');
ylabel('Vertical Position[Y]');
title('Initial Position at Time = 0')
legend('Particle1', 'Particle2', 'Particle3');