for s in GetSources().values():
    Delete(s)
#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'Cone'
cone1 = Cone()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [844, 548]

# show data in view
cone1Display = Show(cone1, renderView1)
# trace defaults for the display properties.
cone1Display.ColorArrayName = [None, '']

# reset view to fit data
renderView1.ResetCamera()

# change solid color
cone1Display.DiffuseColor = [0.0, 0.0, 1.0]

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-0.3107747826481565, 3.1446164723940235, -0.5272057438581309]
renderView1.CameraViewUp = [0.707523985207407, -0.048279210357722646, -0.7050382459153998]
renderView1.CameraParallelScale = 0.8291561935301535

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
