from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

[Delete(s) for s in GetSources().values()]

renderView1 = GetActiveViewOrCreate('RenderView')

# x
cylinderX        = Cylinder()
cylinderX.Radius = 0.02
cylinderX.Center = [0.0, 0.5, 0.0]
cylinderX.Height = 1.0

cylinderXDisplay = Show(cylinderX, renderView1)
cylinderXDisplay.ColorArrayName = [None, '']
cylinderXDisplay.DiffuseColor = [1.0, 0.0, 0.0]
# Default Cylinder is orientated along y-axis.
# To get x cylinder, rotate by -90 around z-axis.
cylinderXDisplay.Orientation = [0.0, 0.0, -90.0]

a3DText1 = a3DText()
a3DText1.Text = 'X'
linearExtrusion1 = LinearExtrusion(Input=a3DText1)
linearExtrusion1Display = Show(linearExtrusion1, renderView1)
linearExtrusion1Display.Scale = [0.1, 0.1, 0.1]
linearExtrusion1Display.Origin = [1,0,0]


# y
cylinderY        = Cylinder()
cylinderY.Radius = 0.02
cylinderY.Center = [0.0, 0.5, 0.0]
cylinderY.Height = 1.0
cylinderYDisplay = Show(cylinderY, renderView1)
cylinderYDisplay.ColorArrayName = [None, '']
cylinderYDisplay.DiffuseColor = [1.0, 1.0, 0.5]
# Default Cylinder is orientated along y-axis, so 
# the following statement is not needed.
cylinderYDisplay.Orientation = [0.0, 0.0, 0.0]

a3DText1 = a3DText()
a3DText1.Text = 'Y'
linearExtrusion1 = LinearExtrusion(Input=a3DText1)
linearExtrusion1Display = Show(linearExtrusion1, renderView1)
linearExtrusion1Display.Scale = [0.1, 0.1, 0.1]
linearExtrusion1Display.Origin = [0,1,0]

# z
cylinderZ        = Cylinder()
cylinderZ.Radius = 0.02
cylinderZ.Center = [0.0, 0.5, 0.0]
cylinderZ.Height = 1.0
cylinderZDisplay = Show(cylinderZ, renderView1)
cylinderZDisplay.ColorArrayName = [None, '']
cylinderZDisplay.DiffuseColor = [0.0, 1.0, 0.0]
# Default Cylinder is orientated along y-axis.
# To get z cylinder, rotate by 90 around x-axis.
cylinderZDisplay.Orientation = [90.0, 0.0, 0.0]

a3DText1 = a3DText()
a3DText1.Text = 'Z'
linearExtrusion1 = LinearExtrusion(Input=a3DText1)
linearExtrusion1Display = Show(linearExtrusion1, renderView1)
linearExtrusion1Display.Scale = [0.1, 0.1, 0.1]
linearExtrusion1Display.Origin = [0,0,1]

cone1 = Cone()
cone1.Radius = 0.05
cone1.Height = 0.2
cone1.Center = [1.0, 0.0, 0.0]
cone1Display = Show(cone1, renderView1)
cone1Display.ColorArrayName = [None, '']
cone1Display.DiffuseColor = [1.0, 0.0, 0.0]

cone2 = Cone()
cone2.Radius = 0.05
cone2.Height = 0.2
cone2.Center = [0.0, 1.0, 0.0]
cone2.Direction = [0.0, 1.0, 0.0]
cone2Display = Show(cone2, renderView1)
cone2Display.ColorArrayName = [None, '']
cone2Display.DiffuseColor = [1.0, 1.0, 0.4980392156862745]

cone3 = Cone()
cone3.Radius = 0.05
cone3.Height = 0.2
cone3.Center = [0.0, 0.0, 1.0]
cone3.Direction = [0.0, 0.0, 1.0]
cone3Display = Show(cone3, renderView1)
cone3Display.ColorArrayName = [None, '']
cone3Display.DiffuseColor = [0.3333333333333333, 1.0, 0.0]

Render()
