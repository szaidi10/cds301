clear;
clf;
x = load('particles3.txt');
time = x(:,1)
particle1 = x(:,5)
particle2 = x(:,9)
particle3 = x(:,13)

a = plot(time, particle1, 'Color', 'Green')
hold on;
b = plot(time, particle2, 'Color', 'Red')
hold on;
c = plot(time, particle3, 'Color', 'Blue')
hold on;
axis([0 2.0 -1.0 1.0])
grid on;
n=get(gca,'xtick');
set(gca,'xticklabel',sprintf('%.1f\n',n'));
xlabel('Time [s]');
ylabel('Vertical Velocity V_y [m/s]');
% Title is redundant IMO
title('Vertical Velocity V_y vs. Time')
legend('Particle1', 'Particle2', 'Particle3');
