clear;
clf;
[X,Y] = meshgrid([-pi:0.1:pi],[-2:0.1:2]);
U = Y;
V = -sin(X);
quiver(X,Y,U,V);

px(1) = 1
py(1) = 1
vx(1) = interp2(X,Y,U,px(1),py(1));
vy(1) = interp2(X,Y,V,px(1),py(1));
h=0.1;
Nsteps = 172
for t = 2:Nsteps
    px(t) = px(t-1) + h*vx(t-1);
    py(t) = py(t-1) + h*vy(t-1);
    
    vx(t) = interp2(X,Y,U,px(t),py(t));
    vy(t) = interp2(X,Y,V,px(t),py(t));
    
    fprintf('px(%d) = %.2f py(%d) = %.2f vx(%d) = %.2f vy(%d)=%.2f\n', t, px(t), t, py(t), t, vx(t), t, vy(t));
end;
hold on;
title('Exit position: x=-0.04159 y = 2) and Nsteps = 172') 
plot(px,py,'g','LineWidth',5)
plot(px,py,'k.','MarkerSize',10);
plot(px(1),py(1),'kx','MarkerSize',15);

[X,Y] = meshgrid([-pi:0.1:pi],[-2:0.1:2]);
U = Y;
V = -sin(X);
quiver(X,Y,U,V);

px(1) = 1
py(1) = 1
vx(1) = interp2(X,Y,U,px(1),py(1));
vy(1) = interp2(X,Y,V,px(1),py(1));
figure()
h=0.01;
Nsteps = 16500
for t = 2:Nsteps
    px(t) = px(t-1) + h*vx(t-1);
    py(t) = py(t-1) + h*vy(t-1);
    
    vx(t) = interp2(X,Y,U,px(t),py(t));
    vy(t) = interp2(X,Y,V,px(t),py(t));
    
    fprintf('px(%d) = %.2f py(%d) = %.2f vx(%d) = %.2f vy(%d)=%.2f\n', t, px(t), t, py(t), t, vx(t), t, vy(t));
end;
hold on;
title('Exit position: x=3.059 y = 0.108) and Nsteps = 16,497') 
plot(px,py,'g','LineWidth',5)
plot(px,py,'k.','MarkerSize',10);
plot(px(1),py(1),'kx','MarkerSize',15);
