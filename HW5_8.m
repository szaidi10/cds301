clear;clf

x = [0:1];
y = [0:1];
for frame = 0:9
   plot(x+frame,y+frame,'-','LineWidth',3,'Color','black');

   % hold on tells MATLAB to not clear figure when next plot command issued.
   %hold on; 

   % drawnow tells MATLAB to render immediately.  If this is not done,
   % and plots happen quickly, MATLAB may wait until things slow down
   % before rendering on the screen - an you may only see the last frame.
   drawnow; 

   % Uncomment the following to make MATLAB wait until you 
   % hit enter to show the next plot.  Useful for debugging.
   input('Continue?');

   % Set axis limits.  See help axis.
   axis([0 10 0 10]);

   % Create a filename for the image.  %02d means "a 2 digit integer that is
   % zero padded, e.g., 01, 02, 03, ...".  No semicolon is given at end
   % so file that is being written is displayed in the console.
   fname = sprintf('lines_%02d.png',frame)

   % "print" the image to a file in png format (the -d means "device").
   % Both words "print" and "device" are anachronisms.
   print('-dpng',fname);
end